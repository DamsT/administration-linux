<span style="color:yellow"> Damien TORRES  
CPLR</span>

&nbsp;

![image](images/centreon.png)

## <span style="color:blue"> **Historique**</span>
----------
Oreon créé par Merethis et basé sur Nagios, voit le jour en 2005 et change de nom en 2007 passant donc de "Oreon" à "Centreon". Cela à cause d'un conflit lié à un "concurrent" nommé "Orion".  

A partir de 2012, il y a une vraie démarcation qui s'installe entre Centreon et Nagios et permet donc une sorte de monté en puissance à la solution Open Source en instaurant de multiples fonctionnalités tel que le fait de pouvoir modifier et recharger la configuration à chaud ou encore l'implémentation et la gestion des timezone.  

En 2013, le mot Nagios est complètement retiré de la solution.  

2015 est marqué par le changement du nom de la société Merethis en Centreon car l'objectif est de faire vivre Centreon et de supporter l'Open Source.  

En 2016, il y a une refonte du nommage des produits. Le nom Centreon regroupant donc l'ensemble des produits qui en sont liés.

&nbsp;

## <span style="color:blue"> **Informations diverses**</span>
----------
Centreon basé sur CentOS, lui-même issu de Red Hat. C'est donc un fork de fork.  
Trois composants Open Source sont au sein de Centreon :
- ***Centreon Web***, l'interface web au travers du serveur Apache
- ***Centreon Engine***, le moteur collectant les informations des services basé sur Nagios
- ***Centreon Broker***, le gestionnaire d'événements

Centreon étant très léger, il peut tourner sur une machine avec seulement un 1 CPU, 1Go de RAM au minimum à adapter en fonction du volume de services et de machines à superviser.  
Par défaut, Centreon est composé d'une multitude de logiciel tel que MariaDB, Apache, GnuTLS, Net-SNMP, openssl, PHP, RRDTools, zlib.

&nbsp;

## <span style="color:blue"> **Supervision**</span>
----------
Centreon est une distribution permettant de faire de la supervision de service au sein d'autre machine tel que qu'un serveur Windows par exemple.  
Centreon va pouvoir récupérer les informations au travers de SNMP.

Cette distribution permet donc de mettre à disposition très rapidement une solution de supervision grâce à un iso déjà complet qui ne nécessite pas l'installation manuelle de l'ensembles des services Centreon.

Centreon va donc permettre de mettre en place des alertes en fonction de paramètre défini à l'avance.
Dans le cas d'un serveur de fichier, au sein de Centreon, il sera possible de monitorer le(s) disque(s) afin de d'avoir un visuel sur la quantité d'espace libre. Cela permettra donc de créer une alerte, par exemple, à 80% d'usage disque.  

Voici un exemple de services qui peuvent être supervisé sur un Windows Server :

![image](images/capture.jpg)

On y retrouve l'état et la quantité du swap, la quantité de RAM utilisé, le pourcentage d'utilisation du CPU, l'état de réponse au ping ainsi que la quantité de mémoire utilisé sur le disque C: 

&nbsp;

## <span style="color:blue"> **Mon utilisation**</span>
----------
J'ai pu utiliser cette solution durant le projet de BTS qui nous demandais de virtualiser une infrastructure type d'entreprise complète et elle devait comprendre un outil de supervision.
De ce fait j'ai pu déployer un Centreon afin de superviser les différentes machines et services de mon infrastructure.  

J'utilise également ce produit au sein de mon entreprise d'alternance afin de superviser les différents services de notre infrastructure.

&nbsp;

## <span style="color:blue"> **Autre utilisation de Linux**</span>
----------
### <span style="color:#167C80"> *- LineageOS*</span>
![image](images/lineageos.png)
> A titre personnel j'ai la ROM de mon téléphone portable afin d'avoir les dernières versions Android sur un smartphone déjà "ancien" (+5 ans).  
Il s'agit d'un Samsung Galaxy Note 8, bloqué sur Android 9 avec la ROM d'origine.  
Actuellement il tourne sur une ROM LineageOS 19.1 (Android 12) qui est un fork de CyanogenMOD.  
Ce téléphone n'étant pas encore supporté par les développeurs de LineageOS, j'ai une version non officiel développé par un indépendant ce faisant appeler Ivan_Meler.  
Android 13 fraichement sortie au mois d'Aout, ce développeur a déjà mis à jour vers cette nouvelle version.  
Les mises à jour de sécurité et correctifs sont déployés par OTA (Over The Air).  
Lorsque j'ai installé cette ROM, elle existait depuis peu et donc beaucoup de chose ne fonctionnait pas ou partiellement ce qui m'a obligé à m'attribué les droits super utilisateur afin d'apporter les changements nécessaires pour mon utilisation.  
Par exemple, changer l'empreinte du téléphone afin qu'il soit reconnu et certifié par le Google Play Store afin de ne pas être limité dans le bon fonctionnement de certaines applications.  
De ce fait, logiciellement il s'agit d'un Google Pixel 3 XL et non d'un Galaxy Note 8.  
Cela étant un détail parmi tant d'autre.

### <span style="color:#FF5400"> *- HiveOS*</span>
![image](images/hiveos.png)
> Pendant un temps, j'ai miné de la cryptomonnaie avec une machine dédiée et afin d'optimiser le fonctionnement de cette machine j'ai décidé d'utiliser un système spécifique prévu pour cette utilisation.  
Pour ce faire, j'ai utilisé la distribution HievOS basé sur Ubuntu mais complétement revu afin d'y implémenter toutes les nécessités pour miner.  
Cet OS est très léger et peut fonctionner sur clé USB.  
Il suffit d'y ajouter un fichier de configuration spécifique à son installation dans une partition qui est créer automatiquement lors de l'écriture de l'image sur le disque, et c'est tout.
Une fois installé c'est au travers d'une interface web que le contrôle et la configuration d'absolument tout ce fait.  
C'est à dire de spécifié quelle monnaie miner en passant par la modification de la tension du processeur graphique de la carte graphique numéro 1, par exemple.  
Voici une image de mon installation :
![image](images/rig.jpg)
Essentiellement du matériel de récupération et de la fabrication maison cette machine était très stable et à tourner sur plus de 3 mois sans aucune coupure ou intervention de ma part.  
J'ai dû l'arrêter pour un nettoyage afin de préserver le matériel.  
A l'heure actuelle, cette machine est à l'arrêt car depuis la mise à jour d'Ethereum vers sa version 2.0 la rentabilité est au plus bas et je rentrais tout juste dans mes frais de consommation.